// *****************************************************************************
// Heat equation Program
// Supporting code file for the NVIDIA Guest Blog Post
// "Porting an application to multi-GPU with Parallel C++"

// Copyright © 2022 University of Geneva
// Authors: Jonas Latt, Karthik Thyagarajan, Sriharan B S
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

// This program is parallelized with C++ parallel algorithms and MPI. You can
// - Run it on a multi-core CPU using C++ standard parallelism.
// - Run it on a CPU cluster using hybrid parallelization.
// - Run it on a GPU using C++ standard parallelism.
// - Run it on a GPU cluster using hybrid parallelization.
//
// FOR MULTI-CORE CPUs
// To compile with g++ or clang, you should install the Intel Threading
// Building Blocks library ("apt install libtbb-dev"). On Visual Studio,
// this is not required.
// Assuming that mpicxx is a wrapper for g++ or clang, compile with
// something like
//
// mpicxx --std=c++17 -O3 -o heatEquation_mpi heatEquation_mpi.cpp -ltbb
//
// and execute in hybrid MPI / multi-threaded mode with something like
//
// mpirun -np 2 -bind-to socket -map-by socket ./heatEquation_mpi

// FOR NVIDIA GPUs
//
// Assuming that mpicxx is a wrapper for nvc++ (it is likely located in
// /opt/nvidia/hpc_sdk/Linux_x86_64/VERSION/comm_libs/mpi/bin/mpicxx )
// compile with something like
//
// mpicxx --std=c++17 -stdpar -O3 -DUSE_NVIDIA_HPC_SDK -o heatEquation_mpi heatEquation_mpi.cpp
//
// and execute with mpirun for multiple GPUs.


// Define the following macro if you are compiling for NVIDIA GPUs.
// It allows faster multi-GPU execution thanks to pinned memory use
// and generated profiler information.
//#define USE_NVIDIA_HPC_SDK

#include "mpi.h"
#include <algorithm>
#include <cassert>
#include <chrono>
#include <cmath>
#include <execution>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <omp.h>
#include <sstream>
#include <utility>

#ifdef USE_NVIDIA_HPC_SDK
#include <nvtx3/nvToolsExt.h>
#endif

using namespace std;
using namespace std::chrono;

const int N = 8192; // The domain has a N x N resolution.
// Maximum time iteration (code stops earlier if convergence is reached).
const int maxIter = 1'000'000; 
const double epsilon = 1.e-5; // Steady-state convergence criterion.
const double dx = 1. / (double)(N - 1); // Discrete cell size.
const double dt = dx * dx; // Discrete time step.
const double D = 0.2; // Diffusion constant.
const double Dconst = D * dt / (dx * dx); // Precomputed numerical constant.
// Terminal output intervals as a number of iterations (-1 means no output).
const int outputFrequency = 200; 
// Image output intervals as a number of iterations (-1 means no output).
const int imageFrequency = -1; 

// This function is used to allocate memory that runs on the device.
// On NVIDIA GPUs, it uses cudaMalloc to make sure the memory is
// pinned to the GPU, for efficient MPI communication.
double* allocateArray(size_t N) {
#ifdef USE_NVIDIA_HPC_SDK
    double* array;
    cudaMalloc(&array, N * sizeof(double));
    return array;
#else
    return new double[N];
#endif
}

void releaseArray(double* array) {
#ifdef USE_NVIDIA_HPC_SDK
    cudaFree(array); array = 0;
#else
    delete [] array; array = 0;
#endif
}

// Map 2D indices on a linear index.
inline size_t IND(size_t iX, size_t iY)
{
    // You should swap iX and iY in the following line if you want to try
    // swapping lines and columns.
    return iY + N * iX;
};

void write_to_bmp(int N, double const* data, int iter, double minval,
    double maxval); // IO declared later

// Set the initial and boundary condition.
void initialize(double* u_ptr, double dx)
{
    // Parallel initialization of u.
    for_each(
        execution::par_unseq, u_ptr, u_ptr + N * N, [u_ptr, dx](double& u) {
            size_t i = &u - u_ptr;
            size_t iX = i / N;
            size_t iY = i % N;
            double x = (double)iX * dx;
            double y = (double)iY * dx;
            u = 0; // Initial value in bulk
            // Left boundary: A sine period.
            if (iX == 0) {
                u = sin(y * 2. * M_PI);
            }
            // Right boundary: A shifted sine period.
            else if (iX == N - 1) {
                u = sin(y * 2. * M_PI + M_PI / 2.);
            }
            // Top and bottom boundary: linear increase
            else if (iY == 0 || iY == N - 1) {
                u = x;
            }
        });
}

double iterate_pieceSTL(double* u_ptr, double* utmp_ptr, double* u_leftColumn,
    double* u_rightColumn, double dx, int N, int NColumnsLocal, int taskId,
    int numTasks)
{
    // This code is non-periodic, and the loop excludes the boundary rows and
    // columns. This is why the the MPI tasks which are on the left and on the
    // right start or end with a different index.
    int lowerX = taskId == 0 ? 1 : 0;
    int upperX = taskId == numTasks - 1 ? NColumnsLocal - 1 : NColumnsLocal;

    // The l2 reduction maybe can be done in a better way to just include the
    // inner doimain
    return transform_reduce(execution::par_unseq, u_ptr,
        u_ptr + N * NColumnsLocal, utmp_ptr, 0.0, plus<double>(),
        [=](double& u1, double& u2) -> double {
            size_t i = &u1 - u_ptr;
            size_t iX = i / N;
            size_t iY = i % N;
            if ((iY > 0 && iY < N - 1) && (iX >= lowerX && iX < upperX)) {
                double u_left
                    = iX > 0 ? u_ptr[IND(iX - 1, iY)] : u_leftColumn[iY];
                double u_right = iX < NColumnsLocal - 1 ? u_ptr[IND(iX + 1, iY)]
                                                        : u_rightColumn[iY];
                u2 = u_ptr[i] * (1. - 4. * Dconst)
                    + Dconst
                        * (u_right + u_left + u_ptr[IND(iX, iY - 1)]
                            + u_ptr[IND(iX, iY + 1)]);
                return (u1 - u2) * (u1 - u2);
            } else
                return 0;
        });
}

void distribute_initial_condition(double* u, double* u_piece,
    double* utmp_piece, int N, int NColumnsLocal, int numTasks)
{
    vector<int> sendCounts(numTasks);
    vector<int> displacements(numTasks);
    for (int i = 0; i < numTasks; ++i) {
        sendCounts[i] = NColumnsLocal * N;
        displacements[i] = i * NColumnsLocal * N;
    }
    MPI_Scatterv(u, sendCounts.data(), displacements.data(), MPI_DOUBLE, u_piece,
        NColumnsLocal * N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Scatterv(u, sendCounts.data(), displacements.data(), MPI_DOUBLE, utmp_piece,
        NColumnsLocal * N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
}

void gather_data(
    double* u, double* u_piece, int N, int NColumnsLocal, int numTasks)
{
    vector<int> receiveCounts(numTasks);
    vector<int> displacements(numTasks);
    for (int i = 0; i < numTasks; ++i) {
        receiveCounts[i] = NColumnsLocal * N;
        displacements[i] = i * NColumnsLocal * N;
    }

    MPI_Gatherv(u_piece, NColumnsLocal * N, MPI_DOUBLE, u, receiveCounts.data(),
        displacements.data(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
}

void communicate_columns(double* u_piece, double* leftColumn,
    double* rightColumn, int N, int NColumnsLocal, int taskId, int numTasks)
{
    int tag = 0;
    MPI_Status status;
    if (taskId > 0) {
        MPI_Sendrecv(u_piece, N, MPI_DOUBLE, taskId - 1, tag, leftColumn,
            N, MPI_DOUBLE, taskId - 1, tag, MPI_COMM_WORLD, &status);
    }
    if (taskId < numTasks - 1) {
        MPI_Sendrecv(u_piece + N * (NColumnsLocal - 1), N, MPI_DOUBLE,
            taskId + 1, tag, rightColumn, N, MPI_DOUBLE, taskId + 1, tag,
            MPI_COMM_WORLD, &status);
    }
}

int main(int argc, char* argv[])
{
    int provided_guarantee = MPI_THREAD_FUNNELED;
    MPI_Init_thread(
        &argc, &argv, MPI_THREAD_FUNNELED, &provided_guarantee);

    int numTasks, taskId;
    MPI_Comm_size(MPI_COMM_WORLD, &numTasks);
    MPI_Comm_rank(MPI_COMM_WORLD, &taskId);

    // On task 0, we allocate the full matrix for pre- and post-processing.
    double* u = 0; // u are the degrees of freedom ("the temperature")
    if (taskId == 0) {
        u = new double[N * N];
    }

    // This is a simple code. It only works if the pieces assigned to all tasks
    // have exactly the same size.
    assert(N % numTasks == 0);
    // Number of columns assigned to the local MPI task.
    int NColumnsLocal = N / numTasks;
    double* u_piece; // A local matrix with "NColumnsLocal" columns.
    double* utmp_piece; // The temporary copy of the local matrix.
    // The communication layers (a column on the left and one on the right) are
    // allocated separately, we are not simply extending u_piece by two columns.
    // In this way, the algorithm works both for a row-major and a column-major
    // code.
    double *leftColumn, *rightColumn;

    u_piece = allocateArray(N * NColumnsLocal);
    utmp_piece = allocateArray(N * NColumnsLocal);
    leftColumn = allocateArray(N);
    rightColumn = allocateArray(N);

    // Create initial condition on MPI task 0.
    if (taskId == 0) {
        initialize(u, dx);
    }

    // Then, dispatch the data to the MPI tasks.
#ifdef USE_NVIDIA_HPC_SDK
    nvtxRangePushA("distribute_initial_condition");
#endif
    distribute_initial_condition(
        u, u_piece, utmp_piece, N, NColumnsLocal, numTasks);
#ifdef USE_NVIDIA_HPC_SDK
    nvtxRangePop();
#endif
    int num_iter = 0;
    int num_image = 0;
    double l2_error = 1.;
    auto start = high_resolution_clock::now();

#ifdef USE_NVIDIA_HPC_SDK
    nvtxRangePushA("Main_While_Loop");
#endif
    while (l2_error > epsilon && num_iter < maxIter) {

#ifdef USE_NVIDIA_HPC_SDK
        nvtxRangePushA("communicate_columns");
#endif
        // Update the ghost cells (the left and the right column).
        communicate_columns(u_piece, leftColumn, rightColumn, N, NColumnsLocal,
            taskId, numTasks);
#ifdef USE_NVIDIA_HPC_SDK
        nvtxRangePop();
#endif

        // Run the computation on the local slice.
#ifdef USE_NVIDIA_HPC_SDK
        nvtxRangePushA("iterate_pieceSTL");
#endif
        double l2_local = iterate_pieceSTL(u_piece, utmp_piece, leftColumn,
            rightColumn, Dconst, N, NColumnsLocal, taskId, numTasks);
#ifdef USE_NVIDIA_HPC_SDK
        nvtxRangePop();
#endif
        // Execute a reduction to compute the l2-norm error.
        MPI_Allreduce(&l2_local, &l2_error, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

        num_iter++;

        swap(u_piece, utmp_piece);

        if (outputFrequency != -1 && num_iter % outputFrequency == 0) {
            auto stop = high_resolution_clock::now();
            auto duration = duration_cast<microseconds>(stop - start);
            double giga_flops = (double)(N * N) * (double)num_iter * 9.
                / (double)duration.count() * 1.e-3;
            if (taskId == 0) {
                cout << "At step " << num_iter << ", l2-norm error = " << setprecision(5)
                     << sqrt(l2_error) << "; performance = " << giga_flops
                     << " GFLOPS" << endl;
            }
        }
        if (imageFrequency != -1 && num_iter % imageFrequency == 0) {
#ifdef USE_NVIDIA_HPC_SDK
            nvtxRangePushA("image_output");
#endif
            gather_data(u, u_piece, N, NColumnsLocal, numTasks);
            if (taskId == 0) {
                write_to_bmp(N, u, num_image++, -1., 1.);
            }
#ifdef USE_NVIDIA_HPC_SDK
            nvtxRangePop();
#endif
        }
    }
#ifdef USE_NVIDIA_HPC_SDK
    nvtxRangePop();
#endif

    delete [] u;
    releaseArray(u_piece);
    releaseArray(utmp_piece);
    releaseArray(leftColumn);
    releaseArray(rightColumn);

    MPI_Finalize();
    return 0;
}

// Write the data to an image in the raw BMP format.
// To convert images to a video: ffmpeg -r 40 -i T_%04d.bmp -q:v 0 heat.mp4
void write_to_bmp(
    int N, double const* data, int iter, double minval, double maxval)
{
    unsigned char bmpfileheader[14]
        = { 'B', 'M', 0, 0, 0, 0, 0, 0, 0, 0, 54, 0, 0, 0 };
    unsigned char bmpinfoheader[40]
        = { 40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 24, 0 };

    int width = N;
    int height = N;

    int padding = (4 - (width * 3) % 4) % 4;

    int datasize = (3 * width + padding) * height;
    int filesize = 54 + datasize;

    vector<unsigned char> img(datasize);

    // A linear interpolation function, used to create the color scheme.
    auto linear = [](double x, double x1, double x2, double y1, double y2) {
        return ((y2 - y1) * x + x2 * y1 - x1 * y2) / (x2 - x1);
    };

    for (int iX = 0; iX < width; iX++) {
        for (int iY = 0; iY < height; iY++) {
            // Restrain the value to be plotted to [0, 1]
            double value
                = ((data[iY + height * iX] - minval) / (maxval - minval));
            double r = 0., g = 0., b = 0.;
            // For good visibility, use a color scheme that goes from black-blue
            // to black-red.
            if (value <= 1. / 8.) {
                r = 0.;
                g = 0.;
                b = linear(value, -1. / 8., 1. / 8., 0., 1.);
            } else if (value <= 3. / 8.) {
                r = 0.;
                g = linear(value, 1. / 8., 3. / 8., 0., 1.);
                b = 1.;
            } else if (value <= 5. / 8.) {
                r = linear(value, 3. / 8., 5. / 8., 0., 1.);
                g = 1.;
                b = linear(value, 3. / 8., 5. / 8., 1., 0.);
            } else if (value <= 7. / 8.) {
                r = 1.;
                g = linear(value, 5. / 8., 7. / 8., 1., 0.);
                b = 0.;
            } else {
                r = linear(value, 7. / 8., 9. / 8., 1., 0.);
                g = 0.;
                b = 0.;
            }

            r = min(255. * r, 255.);
            g = min(255. * g, 255.);
            b = min(255. * b, 255.);

            img[(iX + iY * width) * 3 + iY * padding + 2] = (unsigned char)(r);
            img[(iX + iY * width) * 3 + iY * padding + 1] = (unsigned char)(g);
            img[(iX + iY * width) * 3 + iY * padding + 0] = (unsigned char)(b);
        }
    }

    bmpfileheader[2] = (unsigned char)(filesize);
    bmpfileheader[3] = (unsigned char)(filesize >> 8);
    bmpfileheader[4] = (unsigned char)(filesize >> 16);
    bmpfileheader[5] = (unsigned char)(filesize >> 24);

    bmpinfoheader[4] = (unsigned char)(width);
    bmpinfoheader[5] = (unsigned char)(width >> 8);
    bmpinfoheader[6] = (unsigned char)(width >> 16);
    bmpinfoheader[7] = (unsigned char)(width >> 24);
    bmpinfoheader[8] = (unsigned char)(height);
    bmpinfoheader[9] = (unsigned char)(height >> 8);
    bmpinfoheader[10] = (unsigned char)(height >> 16);
    bmpinfoheader[11] = (unsigned char)(height >> 24);
    bmpinfoheader[20] = (unsigned char)(datasize);
    bmpinfoheader[21] = (unsigned char)(datasize >> 8);
    bmpinfoheader[22] = (unsigned char)(datasize >> 16);
    bmpinfoheader[23] = (unsigned char)(datasize >> 24);

    stringstream filename;
    filename << "T_";
    filename << setfill('0') << setw(4) << iter;
    filename << ".bmp";

    ofstream f;
    f.open(filename.str().c_str(), ios::binary | ios::out);

    f.write((const char*)bmpfileheader, 14);
    f.write((const char*)bmpinfoheader, 40);

    f.write((const char*)&img[0], datasize);
    f.close();
}
